#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TARGET=$DIR/target
ZIP=$TARGET/Lambda.zip
VENV=$TARGET/venv
DEPS=$TARGET/deps

# Clean up old build, create build dir
rm -rf $TARGET
mkdir $TARGET
mkdir $DEPS

# Setup virtualenv
virtualenv $VENV
source $VENV/bin/activate

# Install dependencies
cd $DIR
pip install -r $DIR/requirements_prod
echo "Copying packages into ZIP"
cd $VENV/lib/python2.7/site-packages
zip -r $ZIP *

# Install statically linked dependencies
cd $DIR
OIFS=$IFS
IFS=','
ARR=($(cat requirements_static | awk 'BEGIN {FS="=="} { for(i=1;i<=NF;i+=2){ j=i+1; print ($j" "$i",")}}'))
IFS=$OIFS
cd $DEPS
for i in "${ARR[@]}"
do
  cd $DEPS
  STR_ARR=($i)
  git clone ${STR_ARR[0]} ${STR_ARR[1]}
  cd ${STR_ARR[1]}
  echo "Copying ${STR_ARR[1]} to zip..."
  zip -gr $ZIP * -x "*.md"
done

# Include all .py files in the src directory at the root of the zip
cd $DIR
zip $ZIP -gjr src/*.py

# FINISHED
echo "Your ZIP is ready to go."
echo $ZIP

# Reset
cd $DIR
deactivate
