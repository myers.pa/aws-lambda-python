# Starter package for running a Python based AWS Lambda.

### How to use it
Clone this repository, it will serve as the base directory of your Python based Lambda.  
Add any ```.py``` source files into the ```/src``` directory.

#### Building the ZIP file
Just run ```./build.sh```. This will construct (in the ```/target``` directory) the ZIP file you can upload to AWS Lambda.

#### requirements_dev
This is your standard Python module dependencies file as outlined here: https://pip.pypa.io/en/stable/user_guide/#requirements-files

#### requirements_prod (only used by build script)
Same as requirements_dev, but excludes anything you need statically linked to provide compatibility with AWS Lambda. (ex. psycopg2)

#### requirements_static (only used by build script)
Statically linked dependencies and their git location.  
ex. ```pyscopg2==https://github.com/myerspa/aws-lambda-psycopg2.git```  

Note that this requires you to have Git installed and configured on your system.

#### Prerequisites
This starter package assumes you have the following items installed and configured.
* Python 2.7 (https://www.python.org/download/releases/2.7/)
* pip (https://pip.pypa.io/en/stable/installing/)
* virtualenv (https://virtualenv.pypa.io/en/latest/installation/)
